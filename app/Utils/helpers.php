<?php

if( ! function_exists('api_response') ){
    function api_response($returnData, $isError = false, $status = 404){
        if($isError){
            return response()->json([
                'message' => 'An error has ocurred',
                'errors' => !is_array($returnData) ? [$returnData] : $returnData
            ], $status);
        }

        return response()->json($returnData);
    }
}

if( ! function_exists('public_api_response') ){
    function public_api_response($returnData, $isError = false, $status = 404){
        if($isError) {
            return api_response($returnData, $isError, $status);
        }

        $res = array_merge(
            [
                'data' => [
                    'result' => true,
                ]
            ],
            $returnData
        );

        return response()->json($res);
    }
}


if (!function_exists('log_error')) {
    function log_error($title, \Exception $exception, $data = []) {
        $message = $title;
        $message .= "\n\tException: " . $exception->getMessage();
        foreach ($data as $key => $value) {
            $message .= "\n\t$key: " . json_encode($value);
        }

        \Log::error($message);
    }
}
