<?php

namespace App\Http\Controllers\App;

use App\Churchs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChurchsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('churchsList')){

            $churchs = Churchs::where('name', 'like', '%' . $request->churchsList. '%')->orWhere('vicar','like', '%'. $request->churchsList)->get();
        }else {
            $churchs = Churchs::all();
        }
        return $churchs;
    
    }

    public function create($request)
    {   
       $churchs = new Churchs;

       $churchs ->name          = $request->input('name');
       $churchs ->vicar         = $request->input('vicar');
       $churchs ->address       = $request->input('address');
       $churchs ->neighborhood  = $request->input('neighborhood');
       $churchs ->telephone     = $request->input('telephone');
       $churchs ->parishPriest  = $request->input('parishPriest');

       $churchs->save();
       return response()->json($churchs);

    }

    
    public function store(Request $request)
    {
       
    }

    
    public function show()
    {
        $churchs =  Churchs::all();
        return response()->json($churchs);
    }

    
    public function edit($id)
    {
        $churchs = Churchs::find($id);
        return response()->json($churchs);
    }

    
    public function update(Request $request,$id)
    {   
        $churchs = Churchs::find($id);
        $churchs ->name          = $request->input('name');
        $churchs ->vicar         = $request->input('vicar');
        $churchs ->address       = $request->input('address');
        $churchs ->neighborhood  = $request->input('neighborhood');
        $churchs ->telephone     = $request->input('telephone');
        $churchs ->parishPriest  = $request->input('parishPriest');
        
        $churchs->save();
        return response()->json($churchs);
        
    }

   
    public function destroy($id)
    {
        Churchs::destroy($id);
        return response()->json([
            'res'=>true,
            'message'=>'Registro Eliminado Correctamente']);
    }
}
