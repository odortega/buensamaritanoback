<?php

namespace App\Http\Controllers\App;

use App\Beneficiary;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BeneficiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $beneficiariesList = Beneficiary::get();
        return api_response(["msg"=>true, "data"=>$beneficiariesList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $data = $request->all();
        $beneficiary = new Beneficiary;
        $beneficiary->name =  $data["name"];
        $beneficiary->document_number =  $data["document_number"];
        $beneficiary->phone_number =  $data["phone_number"];
        $beneficiary->is_landline =  $data["is_landline"];
        $beneficiary->save();
        return api_response(["msg"=>true, "data"=>$beneficiary->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $data = $request->all();
        $id = $data["id"];
        $beneficiary = Beneficiary::where('id', $id);
        $beneficiary->update([
            "name" => $data["name"],
            "document_number" => $data["document_number"],
            "phone_number" => $data["phone_number"],
            "is_landline" => $data["is_landline"]
        ]);
        return api_response(["msg"=>true, "data"=>$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
