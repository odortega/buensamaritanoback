<?php

namespace App\Http\Controllers\App;

use App\AssistancesType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssisTypeController extends Controller
{

    public function index(Request $request)
    {   
        if($request->has('assisTypeList')){

            $assisType = AssistancesType::where('name', 'like', '%' . $request->assisTypeList. '%')
                                      ->orWhere('vicar','like', '%' . $request->assisTypeList)->get();
        }else {
            $assisType = AssistancesType::all();
        }
        return $assisType;

    }

    
    public function create($request)
    {   
        $assisType = new AssistancesType();
        $assisType ->name = $request->input('name');

        $assisType->save();
        return response()->json($assisType);

    }

    
    public function store()
    {
        
    }

    
    public function show()
    {
        $assisType =  AssistancesType::all();
        return response()->json($assisType);
    }

  
    public function edit($id)
    {
        $assisType = AssistancesType::findOrFail($id);
        return response()->json($assisType);
    }

    
    public function update(Request $request,$id)
    {
        $assisType = AssistancesType::find($id);
        $assisType ->name = $request->input('name');
        
        $assisType->save();
        return response()->json($assisType);

    }

    
    public function destroy($id)
    {
        AssistancesType::destroy($id);
        return response()->json([
            'res'=>true,
            'message'=>'Registro Eliminado Correctamente']);

    }
}
