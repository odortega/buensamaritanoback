<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UsersController extends Controller
{

    public function index(Request $request)
    {
        $users = User::paginate();
        return api_response('users.index', compact('users'));

    }

    public function show($id)
    {
        $user = User::find($id);
        return api_response('users.show', compact('user'));
        
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get();
        return api_response('users.edit', compact('user', 'roles'));
        
    }

    public function update(Request $request, $id)
    {   

        $user = User::find($id);
        $user->update($request->all());
        $user->roles()->sync($request->get('roles'));

        return redirect()->route('users.edit', $user->id)
            ->with('data', 'Usuario guardado con éxito');
    }

    
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        return back()->with('data', 'Eliminado correctamente');
    }
}
