<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistancesType extends Model
{
    public $table = "assistances_type";
}
