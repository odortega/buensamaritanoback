<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColumnsInAssistancesType extends Migration
{
    /**
     * Run the migrations.
     * Add columns:
     *  name
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assistances_type', function (Blueprint $table) {
            //
            $table->text('name')->nullable(false);;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assistances_type', function (Blueprint $table) {
            //
            $table->dropColumn('name');
        });
    }
}
