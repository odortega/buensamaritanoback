<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConstraintsInAssistances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assistances', function (Blueprint $table) {
            //
            $table->foreign('beneficiary_id')->references('id')->on('beneficiaries');
            $table->foreign('church_id')->references('id')->on('churchs');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assistances', function (Blueprint $table) {
            //
            $table->dropForeign(['beneficiary_id']);
            $table->dropForeign(['church_id']);
        });
    }
}
