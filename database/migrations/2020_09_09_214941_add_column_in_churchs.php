<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInChurchs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('churchs', function (Blueprint $table) {
            //
            $table->string('vicar');
            $table->string('address');
            $table->string('neighborhood');
            $table->string('telephone');
            $table->string('parishPriest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('churchs', function (Blueprint $table) {
            //
            $table->dropColumn('vicar');
            $table->dropColumn('address');
            $table->dropColumn('neighborhood');
            $table->dropColumn('telephone');
            $table->dropColumn('parishPriest');
        });
    }
}
