<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColumnsInChurchs extends Migration
{
    /**
     * Run the migrations.
    * Add columns:
     *  name,
     *  latitude,
     *  longitude
    *
     * @return void
     */
    public function up()
    {
        Schema::table('churchs', function (Blueprint $table) {
            //
            $table->text('name')->nullable(false);
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('churchs', function (Blueprint $table) {
            //
            $table->dropColumn('name');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
