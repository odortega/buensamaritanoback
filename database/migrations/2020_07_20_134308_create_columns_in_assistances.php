<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColumnsInAssistances extends Migration
{
    /**
     * Run the migrations.
     * Add columns:
     *  assistance_type_id,
     *  beneficiary_id,
     *  church_id,
     *  delivered_at,
     *  latitude,
     *  longitude
     * @return void
     */
    public function up()
    {
        Schema::table('assistances', function (Blueprint $table) {
            //
            $table->bigInteger('assistance_type_id')->nullable(false);
            $table->bigInteger('beneficiary_id')->nullable(false)->unsigned()->index();
            $table->bigInteger('church_id')->nullable(true)->unsigned()->index();
            $table->timestamp('delivered_at')->nullable(true);
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assistances', function (Blueprint $table) {
            //
            $table->dropColumn('assistance_type_id');
            $table->dropColumn('beneficiary_id');
            $table->dropColumn('church_id');
            $table->dropColumn('delivered_at');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
