<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColumnsInBeneficiaries extends Migration
{
    /**
     * Run the migrations.
     * Add columns:
     *  document_number,
     *  name,
     *  phone_number,
     *  is_landline,
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiaries', function (Blueprint $table) {
            //
            $table->bigInteger('document_number')->nullable(false);
            $table->text('name')->nullable(false);
            $table->bigInteger('phone_number')->nullable();
            $table->tinyInteger('is_landline')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiaries', function (Blueprint $table) {
            //
            $table->dropColumn('document_number');
            $table->dropColumn('name');
            $table->dropColumn('phone_number');
            $table->dropColumn('is_landline');
       });
    }
}
