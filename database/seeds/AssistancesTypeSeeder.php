<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
use App\AssistancesType;

class AssistancesTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath('database/seeds/assistances_type.csv', 'r');     
        $csv->setDelimiter(';');     
        $csv->setHeaderOffset(0);     
        $csv->addStreamFilter('convert.iconv.UTF-8/UTF-8//TRANSLIT');
        $records = $csv->getRecords();
        
        AssistancesType::getQuery()->delete();
        foreach ($records as $r) {
            $registro = new AssistancesType();
            $registro->name = $r['name'];
            $registro->save();
        }
    }
}
