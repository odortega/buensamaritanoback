<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\CharsetConverter;
use App\Churchs;

class ChurchsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath('database/seeds/churchs.csv', 'r');     
        $csv->setDelimiter(';');     
        $csv->setHeaderOffset(0);     
        $csv->addStreamFilter('convert.iconv.UTF-8/UTF-8//TRANSLIT');
        $records = $csv->getRecords();
       
        Churchs::getQuery()->delete();
        foreach ($records as $r) {
            $churchs = new Churchs();
            $churchs->name = $r['name'];
            $churchs->vicar = $r['vicar'];
            $churchs->address = $r['address'];
            $churchs->neighborhood = $r['neighborhood'];
            $churchs->telephone = $r['telephone']; 
            $churchs->parishPriest = $r['parishPriest'];
            $churchs->save();
        }
    }
}
