<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    

         //Users
         Permission::create([
            'name'          => 'Navegar usuarios',
            'slug'          => 'users.index',
            'description'   => 'Lista y navega todos los usuarios del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de usuario',
            'slug'          => 'users.show',
            'description'   => 'Ve en detalle cada usuario del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Edición de usuarios',
            'slug'          => 'users.edit',
            'description'   => 'Podría editar cualquier dato de un usuario del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar usuario',
            'slug'          => 'users.destroy',
            'description'   => 'Podría eliminar cualquier usuario del sistema',      
        ]);

        //Roles
        Permission::create([
            'name'          => 'Navegar roles',
            'slug'          => 'roles.index',
            'description'   => 'Lista y navega todos los roles del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de un rol',
            'slug'          => 'roles.show',
            'description'   => 'Ve en detalle cada rol del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de roles',
            'slug'          => 'roles.create',
            'description'   => 'Podría crear nuevos roles en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de roles',
            'slug'          => 'roles.edit',
            'description'   => 'Podría editar cualquier dato de un rol del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar roles',
            'slug'          => 'roles.destroy',
            'description'   => 'Podría eliminar cualquier rol del sistema',      
        ]);

        //Churchs
        Permission::create([
            'name'          => 'Navegar Parroquia',
            'slug'          => 'churchs.index',
            'description'   => 'Lista y navega todos las parroquias del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de una parroquia',
            'slug'          => 'churchs.show',
            'description'   => 'Ve en detalle cada parroquia del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de parroquias',
            'slug'          => 'churchs.create',
            'description'   => 'Podría crear nuevas parroquias en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de parroquias',
            'slug'          => 'churchs.edit',
            'description'   => 'Podría editar cualquier dato de parroquia del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar parroquias',
            'slug'          => 'churchs.destroy',
            'description'   => 'Podría eliminar cualquier parroquia del sistema',      
        ]);

        //Beneficiary
        Permission::create([
            'name'          => 'Navegar Beneficiario',
            'slug'          => 'beneficiary.index',
            'description'   => 'Lista y navega todos los beneficiarios del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de un beneficiario',
            'slug'          => 'beneficiary.show',
            'description'   => 'Ve en detalle cada beneficiario del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de beneficiarios',
            'slug'          => 'beneficiary.create',
            'description'   => 'Podría crear nuevos beneficiarios en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de beneficiarios',
            'slug'          => 'beneficiary.edit',
            'description'   => 'Podría editar cualquier dato de un beneficiario del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar beneficiarios',
            'slug'          => 'beneficiary.destroy',
            'description'   => 'Podría eliminar cualquier beneficiario del sistema',      
        ]);

        //AssistanceType
        Permission::create([
            'name'          => 'Navegar Tipo de Asistencia',
            'slug'          => 'assistanceType.index',
            'description'   => 'Lista y navega todos los tipos de asistencia del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de Tipo de asistencia',
            'slug'          => 'assistanceType.show',
            'description'   => 'Ve en detalle de tipo de asistencia del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de Tipo de asistencias',
            'slug'          => 'assistanceType.create',
            'description'   => 'Podría crear nuevos tipo de asistencias en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de Tipo de asistencia',
            'slug'          => 'assistanceType.edit',
            'description'   => 'Podría editar cualquier dato de tipo de asistencia del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar tipo de asistencia',
            'slug'          => 'assistanceType.destroy',
            'description'   => 'Podría eliminar cualquier tipo de asistencia del sistema',      
        ]);


    }
}