<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');


    Route::group(['middleware' => 'auth:api'], function() {
        //GET method Routes
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::get('beneficiaries/listBeneficiaries', 'App\BeneficiaryController@index');
        Route::get('assistances/listAssistances', 'App\AssistanceController@index');
        Route::get('assistance_types/listAssistanceTypes', 'App\AssistanceTypeController@index');
        Route::get('churchs/listChurchs', 'App\ChurchController@index');
        //POST method Routes
        Route::post('beneficiaries/createBeneficiary', 'App\BeneficiaryController@create');
        Route::post('beneficiaries/updateBeneficiary', 'App\BeneficiaryController@update');
    });
});

