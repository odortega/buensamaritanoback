<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return csrf_token();
});

//Route::middleware(['auth'])->group(function () {

    //Roles
    Route::get('roles', 'App\RoleController@index')->name('roles.index')
        ->middleware('permission:roles.index');

    Route::get('roles/create', 'App\RoleController@create')->name('roles.create')
        ->middleware('permission:roles.create');

    Route::post('roles/store', 'App\RoleController@store')->name('roles.store')
        ->middleware('permission:roles.create');

    Route::get('roles/{role}', 'App\RoleController@show')->name('roles.show')
        ->middleware('permission:roles.show');
        
    Route::get('roles/{role}/edit', 'App\RoleController@edit')->name('roles.edit')
        ->middleware('permission:roles.edit');

    Route::put('roles/{role}', 'App\RoleController@update')->name('roles.update')
        ->middleware('permission:roles.edit');

    Route::delete('roles/{role}', 'App\RoleController@destroy')->name('roles.destroy')
        ->middleware('permission:roles.destroy');

    //Users
    Route::get('users', 'UserController@index')->name('users.index')
        ->middleware('permission:users.index');

    Route::get('users/{user}', 'UserController@show')->name('users.show')
        ->middleware('permission:users.show');
        
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
        ->middleware('permission:users.edit');
        
    Route::put('users/{user}', 'UserController@update')->name('users.update')
        ->middleware('permission:users.edit');
        
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
        ->middleware('permission:users.destroy');

    //Churchs

    Route::get('churchs', 'App\ChurchsController@index')->name('churchs.index')
        ->middleware('permission:churchs.index');

    Route::post('churchs/create', 'App\ChurchsController@create')->name('churchs.create')
		->middleware('permission:churchs.create'); 

    Route::post('churchs/store', 'App\ChurchsController@store')->name('churchs.store')
		->middleware('permission:churchs.create');
    
    Route::get('churchs/{church}', 'App\ChurchsController@show')->name('churchs.show')
		->middleware('permission:churchs.show');

	Route::put('churchs/{church}', 'App\ChurchsController@update')->name('churchs.update')
        ->middleware('permission:churchs.edit');
    
    Route::get('churchs/{church}/edit', 'App\ChurchsController@edit')->name('churchs.edit')
		->middleware('permission:churchs.edit');

	Route::delete('churchs/{church}', 'App\ChurchsController@destroy')->name('churchs.destroy')
		->middleware('permission:churchs.destroy');

	
//});